var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
Login = mongoose.model("loginSchema");
Order = mongoose.model("orderSchema");
Product=mongoose.model("productSchema");
Company =mongoose.model("companySchema");
Allotment =mongoose.model("allotmentSchema");
AllotmentOption = mongoose.model("allotmentOptionSchema");
Payment = mongoose.model("paymentSchema");
Expense = mongoose.model("expenseSchema");
json2xls = require("json2xls");
var jwt = require("jsonwebtoken");
var moment = require("moment");
var fs = require("fs");
var http = require('http');
var moment = require('moment');
var AlotArray = [];
var finalAlot = [];
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');

// order1=new Order();
// order1.save(function (err) {

//     // book._id === 100 -> true

//     order1.nextCount(function(err, count) {

//         // count === 101 -> true

//         order1.resetCount(function(err, nextCount) {

//             // nextCount === 100 -> true

//         });
//     });
// });



// exports.findUser = function(req,res){
// Login.find({user:req.params.user,password:req.params.password},function(err,Login){
//
// 	 if(err)
// 	 res.send(err);
// 	 res.json(Login);
//
//
// });
// };

// var json =[ { _id: "59425325c6b20d2e5abd4ca2",
//     type: 'food',
//     paidFor: 'khanaJana',
//     rate: 2313,
//     createdAt: "2017-06-15T09:28:05.445Z" },
//   { _id: "59425332c6b20d2e5abd4ca3",
//     type: 'food',
//     paidFor: 'khoobkhana',
//     rate: 230,
//     createdAt: "2017-06-15T09:28:18.696Z" } ];
//
// var xls = json2xls(json);
// fs.writeFileSync('data.xlsx', xls, 'binary');
// console.log("file saved");


exports.createUser = function(req,res) {
	var newUser = new Login(req.body);
	newUser.save(function(err,login) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Account created successfully' });
	});
};

// product API

exports.createProduct = function(req,res) {
	var newProduct = new Product(req.body);
	newProduct.save(function(err,product) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Product created successfully' });
	});
};

exports.findProductQuery = function(req,res){

    var data = req.body.name;
	  Product.find({name: new RegExp(data, 'i')},function(err,product) {

			if(err)
			res.send(err);
			res.send(product);


	  });


};



exports.showAllProducts = function(req,res) {
       Product.find({},function(err,product){
            if(err)
            res.send(err)
            res.send(product);

       });

};


exports.deleteProduct = function(req,res){
  Product.remove({_id:{$in:req.body}},function(err,product) {
  	if(err)
		res.send(err);
		res.json({ success: true, message: 'product deleted successfully' });
  });
};


	exports.updateProduct = function(req,res){

     Product.update({_id:req.body._id},{name:req.body.name,rate:req.body.rate},{upsert:false,safe:true},function(err,product) {
			 if(err)
			 res.send(err);
	 		res.json({ success: true, message: 'product updated successfully' });

     });


	};

//***********************************************company API start******************************************************


exports.createCompany = function(req,res) {
	var newCompany = new Company(req.body);
	newCompany.save(function(err,Company) {
		if(err)
		res.send(err);
		res.json({ success: true, message: 'Company created successfully' });
	});
};

exports.showAllCompany = function(req,res) {
       Company.find({},function(err,company){

            if(err)
            res.send(err)
            res.send(company);

       });

};


exports.deleteCompany = function(req,res){
  Company.remove({_id:{$in:req.body}},function(err,company) {
  	if(err)
		res.send(err);
		res.json({ success: true, message: 'product deleted successfully' });
  });
};


	exports.updateCompany = function(req,res){

     Company.update({_id:req.body._id},{name:req.body.name,address:req.body.address,phone:req.body.phone},{upsert:true,safe:true},function(err,company) {
			 if(err)
			 res.send(err);
	 		res.json({ success: true, message: 'product updated successfully' });

     });


	};

exports.getCompanyById = function(req,res){
	Company.find({_id:req.body.id},function(err,company){
		if (err) {
			res.send(err);
		}
		res.send(company);
	});
};

	exports.findCompanyQuery = function(req,res){

	    var data = req.body.name;
		  Company.find({name: new RegExp(data, 'i'),route:req.body.route},function(err,product) {

				if(err)
				res.send(err);
				res.send(product);

		  });

	};


	exports.updateBalance = function(req,res) {

		Company.update({_id:req.body._id},{ $inc: { balance:req.body.balance} },{upsert:false,safe:true},function (err,company){
			if(err)
			res.send(err);
		 res.json({ success: true, message: 'Balance updated successfully' });
	 });
 };


 exports.updateBalanceDec = function(req,res) {

 	Company.findOneAndUpdate({_id:req.body._id},{ $inc: { balance:-req.body.balance} },{upsert:false,safe:true},function (err,company){

		 if(req.body.balance>company.balance){
			 Company.update({_id:company._id},{balance:0},function(err,company1) {
        if (err) {
        	console.log(err);
        }

			 });

		 }


			if(err)
			 res.send(err);
			res.json({ success: true, message: 'Balance updated successfully' });


  });
 };

// exports.getCompanyByName = function(req,res) {
// Company.find({name:req.body.companyName},function(err,company) {
// 	if (err) {
// 		res.send(err);
// 	}
// 	else {
// 		res.send(company)
// 	}
//
// });
// };




//end

//**********************************************Payment API start************************************************************

exports.createPayment = function(req,res) {

var newPayment = new Payment(req.body);
newPayment.save(function(err,payment) {
	if(err)
	res.send(err);
	res.json({ success: true, message: 'Payment updated  successfully' });
	res.send(payment);
});

};

exports.getAllPayment = function(req,res){
 Payment.find({},function(err,payment) {
	 if(err)
 	res.send(err);
	res.send(payment);

 });

};

exports.updatePayment = function(req,res) {
Payment.update({_id:req.body._id},{status:req.body.status},{upsert:false},function(err,payment) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Payment updated  successfully' });

});

};





//**********************************************payment API end**************************************************************

//**********************************************Allotment API start************************************************************

exports.createAllotmentProduct = function(req,res) {
 var newAllotment = new Allotment(req.body);
	newAllotment.save(function(err,allotment){
		 if(err)
		 res.send(err);
		 res.json({ success: true, message: 'Allotment product created successfully' });
	});
};

exports.showAllotments = function(req,res) {
	Allotment.find({},function(err,allotment){

			  if (err)
			  res.send(err)
			  res.send(allotment)

	});
};

exports.updateAllotmentProduct = function(req,res) {
Allotment.update({_id:req.body._id},
		{
		$push:{
		allotment:
		{
			supplier:req.body.supplier,
			container:req.body.container,
			rate:req.body.rate,
			quantityDispatched:req.body.quantityDispatched,
			mark:req.body.mark,
			vendor:req.body.vendor,
			status:req.body.status,
			product:req.body.product
		}

}

},{upsert:true},function(err,allotment) {
		if(err)
		res.send(err)
		res.json({ success: true, message: 'Allotment created successfully' });
	});
};

exports.createAllotmentOption = function(req,res){
	var newAllotmentOption = new AllotmentOption(req.body);
	newAllotmentOption.save(function(err,AllotmentOption) {

		if(err)
			res.send(err);
			res.json({ success: true, message: 'Input done successfully' });
	});
};

exports.getAllotmentOption = function(req,res) {
  AllotmentOption.find({},function(err,allotment) {

		 if(err)
		 res.send(err);
		 res.send(allotment);

  });
};

exports.getAllotmentById = function(req,res) {
	Allotment.find({_id:req.body},function(err,allotment) {
		if(err)
		res.send(err);
		res.send(allotment);
	});
};

exports.updateAllotmentById = function(req,res) {
	Allotment.update({_id:req.body.id,'allotment._id':req.body.idA},{$set:{'allotment.$.status':req.body.status,'allotment.$.recieved':new Date()}},{upsert:false},function(err,product){
		if(err)
		res.send(err);
	 res.json({ success: true, message: 'Allotment updated successfully' });

	});
};



exports.getAllotmentBySupplier = function(req,res){
	Allotment.aggregate([
	{
	      $project: {
	         allotment: {
	            $filter: {
	               input: "$allotment",
	               as: "allotment",
	               cond: { $and: [
								           { $gte: [ "$$allotment.created", new Date(req.body.startAt)]},
										   { $lte: [ "$$allotment.created", new Date(req.body.endAt)]},
											{  $eq:["$$allotment.supplier",req.body.supplier] }
			                   ],


	            }
	         }
	      }
	   }
	 }],function(err,allotment){
 		if(err)
 		{res.send(err);}
		else {
			if(allotment[0].allotment.length === 0)
			{
				res.json({success: false, message: 'no data found'})
			}

			else
			{       AlotArray = allotment;
							 for(i in AlotArray )
							 {
								for(j in AlotArray[i].allotment)
								AlotArray[i].allotment[j].created = moment(AlotArray[i].allotment[j].created).format('MM/DD/YYYY');
								AlotArray[i].allotment[j].recieved = moment(AlotArray[i].allotment[j].recieved).format('MM/DD/YYYY');
							 	finalAlot.push(AlotArray[i].allotment[j]);

							 }

							 // console.log();
						 	 // res.send(finalAlot);
						 	 var xls = json2xls(finalAlot);
						     fs.writeFileSync('data.xlsx', xls, 'binary');
						     console.log("file saved");
						     res.download('data.xlsx',function(err){
						     	if(err){
						     		console.log(err);
						     	}
						     });
			}


            }

 });

};

var totalQuantity = {
	                    status:"Total Product in Transit",
						quantityDispatched:0
					}



exports.getAllotmentByStatus = function(req,res){
	Allotment.aggregate([
	{
	      $project: {
	         allotment: {
	            $filter: {
	               input: "$allotment",
	               as: "allotment",
	               cond: { $and: [

									 {  $eq:["$$allotment.status",req.body.status] }
			                   ],


	            }
	         }
	      }
	   }
	 }],function(err,allotment){
 		if(err)
 		{res.send(err);}
		else {

				 AlotArray = allotment;
				 for(i in AlotArray )
				 {
					for(j in AlotArray[i].allotment)
					AlotArray[i].allotment[j].created = moment(AlotArray[i].allotment[j].created).format('MM/DD/YYYY');
					AlotArray[i].allotment[j].recieved = moment(AlotArray[i].allotment[j].recieved).format('MM/DD/YYYY');
					totalQuantity.quantityDispatched = totalQuantity.quantityDispatched+AlotArray[i].allotment[j].quantityDispatched;
				 	finalAlot.push(AlotArray[i].allotment[j]);
				 }
				 finalAlot.push(totalQuantity);

				 // console.log();
			 	//  res.send(finalAlot);
			 	   var xls = json2xls(finalAlot);
			     fs.writeFileSync('data.xlsx', xls, 'binary');
			     console.log("file saved");
			     res.download('data.xlsx');


            }

 });

};


exports.killTheDb = function (req,res) {

	db = mongoose.createConnection(config.database);

  db.dropDatabase(function(err) {
		if(err)
		res(err);
		res.json({ success: true, message: 'database deleted successfully' });

  });


};




//**********************************************Allotment API end************************************************************

//**********************************************Expense API Start************************************************************

exports.createExpense = function(req,res) {
var expense = new Expense(req.body);
expense.save(function(err,expense){
	if(err)
	res.send(err);
	res.json({ success: true, message: 'Expense created successfully' });
});
};

exports.getExpenses = function(req,res) {
  Expense.find({},function(err,expense) {

		 if(err)
		 res.send(err);
		 res.send(expense);

  });
};


exports.getExpensesByType = function(req,res) {
  Expense.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "type":{$eq:req.body.type}
					     },{_id:0,__v:0},{lean:true},function (err,expense) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in expense){
													expense[i].created = moment(expense[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(expense);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};




//**********************************************Expense API end************************************************************

//**********************************************Order API start************************************************************


exports.createOrder = function(req,res){
	 var newOrder = new Order(req.body);
	 newOrder.save(function(err,orderSchema) {
		 if(err)
		 res.send(err);
		 res.json({ success: true, message: 'order created successfully' });
	 });
};

exports.getAllOrders = function(req,res) {
	Order.find({},null,{sort:{"_id":-1}},function(err,order) {
    if(err)
		res.send(err);
		res.send(order);

	});
};

exports.getUserOrder = function(req,res) {

	Order.find({user:req.body.user},null,{sort:{"_id":-1}},function(err,order) {

		if(err)
	 res.send(err);
	 res.send(order);

	});

};

exports.updateOrderTele = function(req,res) {
Order.update({_id:req.body.id},{status:req.body.status,dateModified:req.body.dateModified},{upsert:false},function(err,order) {

 if(err)
 res.send(err);
 res.json({ success: true, message: 'Order Status updates  successfully' });

});
};

exports.getOrderByQuery = function(req,res) {


if (req.body.startAt==null&&req.body.endAt==null) {

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus}},function(err,order){

						 if(err)
						 res.send(err);
						 res.send(order);

					 });

}

else if(req.body.startAt==null){

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						"createdAt": {$lte:new Date(req.body.endAt)}

					},function(err,order){

						if(err)
						res.send(err);
						res.send(order);

					});


}

else if(req.body.endAt==null){

	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						"createdAt": {$gte:new Date(req.body.startAt)}

					},function(err,order){

						if(err)
						res.send(err);
						res.send(order);

					});


}


else{


	Order.find({status:{ $regex:req.body.status},paymentStatus:{ $regex:req.body.paymentStatus},


						 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)}

					 },function(err,order){

						 if(err)
						 res.send(err);
						 res.send(order);

					 });
}


};

exports.getOrderByCompanyId = function(req,res){
Order.find({company_id:req.body.companyId},null,{sort: {'_id': -1}},function(err,order) {
	if(err)
	res.send(err);
  res.send(order);
});

};

exports.getOrderById = function(req,res) {
Order.find({_id:req.body.id},function(err,order) {
	   if(err)
		 res.send(err);
		 res.send(order);
});


};

exports.updateOrderStatus = function(req,res) {
Order.update({_id:req.body.id},{status:req.body.status},{upsert:false},function(err,order) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Order Status updates  successfully' });

});

};

exports.updateOrder = function(req,res) {
Order.update({_id:req.body.id},{paymentRecievedAt:new Date(req.body.paymentRecievedAt),
	    paymentType:req.body.paymentType,
	    paymentStatus:req.body.paymentStatus,
			orderType:req.body.orderType,
		   status:req.body.status,dateModified:req.body.dateModified},{upsert:false},function(err,order) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Order Status updates  successfully' });

});
};

exports.updateOrderPaymentStatus = function(req,res) {
Order.update({_id:req.body.id},{paymentStatus:req.body.status},{upsert:false},function(err,order) {

	if(err)
	res.send(err);
	res.json({ success: true, message: 'Order Payment Status updated  successfully' });

});

};

exports.getOrderByUser = function(req,res) {
  Expense.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "user":{$eq:req.body.user}
					     },{_id:0,__v:0},{lean:true},function (err,expense) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in expense){
													expense[i].created = moment(expense[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(expense);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};



exports.assignUpdate = function(req,res) {
Order.update({_id:req.body.id},{assignedTo:req.body.name,status:req.body.status},{upsert:false},function(err,order) {

	if(err)
 res.send(err);
 res.json({ success: true, message: 'Order Payment Status updated  successfully' });


});

};


exports.getOrderReport = function(req,res) {
  Order.find(
		           {
								 "createdAt": { $gte:new Date(req.body.startAt),$lte:new Date(req.body.endAt)},
			           "user":{$eq:req.body.user}
					     },{_id:0,__v:0},{lean:true},function (err,order) {

						   if(err)
							 {
								 res.send(err);

							 }

							 else
							 {



												for(i in order){
													order[i].createdAt = moment(order[i].created).format('MM/DD/YYYY');
												}



								      var xls = json2xls(order);
								      fs.writeFileSync('data2.xlsx', xls, 'binary');
								      console.log("file saved");
								      res.download("data2.xlsx");

							 }

					});

			};






//**********************************************Order API end************************************************************


exports.authenticate = function(req,res){
	console.log(req.body);
  Login.findOne({
		user:req.body.user
	},function(err,user){
    console.log(user);

		if(err)
		res.send(err);

		if(!user){
			res.json({success:false,message:"Authentication Failed! user not found!"});
		} else if (user) {

			if(user.password != req.body.password)
			{
				res.json({success:false,message:"Authentication Failed! Wrong Password!"});
			} else{

        var token = jwt.sign(user,app.get('superSecret'),{
				 	expiresIn:'1d'
				 });

				res.json({
          success: true,
          message: 'Enjoy your token!',
          role:user.role,
					user:user.user,
					token: token,

        });

			}
		}


	});






};









//
// exports.showUser = function(req,res) {
// 	Login.find({},function(err,user){
// 	if(err)
// 	res.send(err);
// 	res.send(user);
// 	});
// };
