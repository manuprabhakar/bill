var express = require("express");
app = express();
port = process.env.PORT||3000;

var MongoClient = require('mongodb').MongoClient;

var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');

var mongooseBack = require("mongoose");

var json2xls = require('json2xls');

var jwt = require("jsonwebtoken");
Login =  require("./model/loginModal");
Order  = require("./model/orderModal");
Product = require("./model/productModal");
Company = require("./model/companyModal");
Alottment = require("./model/allotmentModal");
AlottmentOption = require("./model/allotmentOptionmodal");
Expense = require("./model/expenseModal");
Payment = require("./model/paymentsModal");


bodyParser = require('body-parser');

var config = require('./config');


mongoose.Promise = global.Promise;
mongoose.connect(config.database);
app.use(express.static(__dirname +'/public'));  // set the static files location /public/img will be /img for users

app.set('superSecret',config.secret);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


setInterval(function(){

  db = mongoose.createConnection('mongodb://localhost:27017/copyBase');

  db.dropDatabase();



  console.log("db dropped");

MongoClient.connect(config.database, function(err, db) {
    if(err) {
        console.log(err);
    }
    else {

        var mongoCommand = { copydb: 1, fromhost: "localhost", fromdb: "tododb", todb: "copyBase" };
        var admin = db.admin();

        admin.command(mongoCommand, function(commandErr, data) {
            if (!commandErr) {
                console.log(data);
            } else {
                console.log(commandErr.errmsg);
            }
            db.close();
        });
    }
});}, 1000*60*60*12);


//test start


// var printer = require("printer");
// filename ="printImages/new.PDF";
//
// console.log('platform:', process.platform);
// console.log('try to print file: ' + filename);
//
// if( process.platform != 'win32') {
// printer.printFile({filename:filename,
// printer: process.env[3], // printer name, if missing then will print to default printer
// success:function(jobID){
//   console.log("sent to printer with ID: "+jobID);
// },
// error:function(err){
//   console.log(err);
// }
// });
// } else {
// // not yet implemented, use printDirect and text
// var fs = require('fs');
// printer.printDirect({data:fs.readFileSync(filename),
// printer: process.env[3], // printer name, if missing then will print to default printer
// success:function(jobID){
//   console.log("sent to printer with ID: "+jobID);
// },
// error:function(err){
//   console.log(err);
// }
// });
// }
//
// util = require('util');
// console.log("supported formats are:\n"+util.inspect(printer.getSupportedPrintFormats(), {colors:true, depth:10}));
//
//
//test end

var routes = require('./routes/route');
routes(app);

app.get('/', function(req, res){
  res.redirect('/index.html');
});
var http = require('http');


app.listen(port);
console.log("app started at port 3000");
