var mongoose = require("mongoose");

var Schema = mongoose.Schema;
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb://test:test@localhost:27017/tododb");

autoIncrement.initialize(connection);

var productQuantitySchema = new Schema({

   name:{type:String},
	 quantity:{type:Number},
   rate:{type:Number}


});

var orderSchema = new Schema({
   orderId:{type:Number, index:{unique: true }},
   user:String,
   route:String,
	 companyName:{type:String},
	 product:[productQuantitySchema],
   paymentType:{type:String, default:"notSelected"},
   totalPrice:Number,
   createdAt:{type:Date ,default:Date.now},
   orderType: {type:String ,default:'notSelected'},
   assignedTo: {type:String ,default:'notAssigned'},
   status:{type:String,default:'pending'},
   paymentStatus:{type:String,default:'notRecieved'},
   company_id:Schema.Types.ObjectId,
   paymentRecievedAt:{type:Date,default:Date.now},
   dateModified:{type:Date,default:Date.now}
});

var order = connection.model('order', orderSchema);

orderSchema.plugin(autoIncrement.plugin, {
    model: 'order',
    field: 'orderId',
    startAt: 2316,
    incrementBy: 1
});

module.export = mongoose.model('orderSchema',orderSchema);
