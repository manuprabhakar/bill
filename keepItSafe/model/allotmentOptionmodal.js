var mongoose = require("mongoose");

var Schema = mongoose.Schema;


var allotmentOptionSchema = new Schema({
 roleName:String,
 name:String,
});

module.export = mongoose.model('allotmentOptionSchema',allotmentOptionSchema);
