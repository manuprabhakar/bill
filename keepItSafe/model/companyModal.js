var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var companySchema = new Schema({
 name:String,
 address:String,
 phone:Number,
 gstin:String,
 state:String,
 stateCode:Number,
 balance:{type:Number,default:0},
 cgst:{type:Number,default:9},
 sgst:{type:Number,default:9},
 igst:{type:Number,default:18}
});

module.export = mongoose.model('companySchema',companySchema);
