var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var loginSchema = new Schema({

 user: String,
 password: String,
 role:String

});

module.export = mongoose.model('loginSchema',loginSchema);
