 module.exports = function(app) {
 var jwt = require("jsonwebtoken");
 var appControl  = require('../controllers/appController');
 var mongoose = require("mongoose");
 Product = mongoose.model("productSchema");


 //API to create a user
     app.route('/api/createUser')
        .post(appControl.createUser);


 //API to authenticate/check user
      app.route("/api/authenticate")
         .post(appControl.authenticate);



 //function to authenticate a request before sending any data
 		app.use(function(req, res, next) {

 	  // check header or url parameters or post parameters for token
 	  var token = req.body.token || req.query.token || req.headers['x-access-token']||req.headers.authorization;
     // console.log(token);
 	  // decode token
 	  if (token) {

 	    // verifies secret and checks login
 	    jwt.verify(token, app.get('superSecret'), function(err, decoded) {
 	      if(err) {
 	        return res.json({ success: false, message: 'Failed to authenticate token.' });
 	      } else {
 	        // if everything is good, save to request for use in other routes
 	        req.decoded = decoded;
 	        next();
 	      }
 	    });

 	  } else {

 	    // if there is no token
 	    // return an error
 	    return res.status(403).send({
 	        success: false,
 	        message: 'No token provided.'
 	    });
 	  }
 	});


   //API to create a new company
      app.route("/api/createCompany")
        .post(appControl.createCompany);


   //
 	// app.route('/user')
   //    .get(Login.showUser);








//*************************************** PRODUCT ROUTES ******************************************************************

app.route("/api/findProductQuery")
   .post(appControl.findProductQuery)



//API to create a new product
   app.route("/api/createProduct")
     .post(appControl.createProduct);

//API to get all order form db
   app.route("/api/showAllProducts")
       .get(appControl.showAllProducts);


//API to Delete a product
  app.route("/api/deleteProduct")
  .post(appControl.deleteProduct);

//API to update a product
   app.route("/api/updateProduct")
      .post(appControl.updateProduct);

//*************************************** COMPANY ROUTES ******************************************************************


      //API to get all company form db
         app.route("/api/showAllCompany")
             .get(appControl.showAllCompany);


      //API to Delete a company
        app.route("/api/deleteCompany")
        .post(appControl.deleteCompany);

      //API to update a company
         app.route("/api/updateCompany")
            .post(appControl.updateCompany);



        app.route("/api/findCompanyQuery")
           .post(appControl.findCompanyQuery);


        app.route("/api/updateBalance")
           .post(appControl.updateBalance);

           app.route("/api/updateBalanceDec")
              .post(appControl.updateBalanceDec);

          app.route("/api/getCompanyById")
              .post(appControl.getCompanyById);

//*************************************** COMPANY ROUTES ******************************************************************

//*************************************** PAYMENT ROUTES ******************************************************************

  //  app.route("/api/createPayment")
  //     .post(appControl.createPayment);
  //
  // app.route("/api/updatePayment")
  //    .post(appControl.updatePayment);
  //
  //    app.route("/api/getAllPayment")
  //       .post(appControl.getAllPayment);




//*************************************** PAYMENT ROUTES ******************************************************************

//***************************************  ALLOTMENT ROUTES ******************************************************************

app.route("/api/createAllotmentProduct")
   .post(appControl.createAllotmentProduct);

app.route("/api/showAllotments")
   .get(appControl.showAllotments);

app.route("/api/updateAllotmentProduct")
   .post(appControl.updateAllotmentProduct);

app.route("/api/createAllotmentOption")
   .post(appControl.createAllotmentOption);

app.route("/api/getAllotmentOption")
   .get(appControl.getAllotmentOption);

app.route("/api/getAllotmentById")
   .post(appControl.getAllotmentById);

app.route("/api/updateAllotmentById")
   .post(appControl.updateAllotmentById);

app.route("/api/getAllotmentBySupplier")
  .post(appControl.getAllotmentBySupplier);


app.route("/api/getAllotmentByStatus")
  .post(appControl.getAllotmentByStatus);

 app.route("/api/killTheDb")
    .get(appControl.killTheDb);

//***************************************  ALLOTMENT ROUTES ******************************************************************

//***************************************  EXPENSE ROUTES ******************************************************************


app.route("/api/createExpense")
  .post(appControl.createExpense);

app.route("/api/getExpenses")
   .get(appControl.getExpenses);

app.route("/api/getExpensesByType")
   .post(appControl.getExpensesByType);



//***************************************  EXPENSE ROUTES ******************************************************************

//***************************************  ORDER ROUTES ******************************************************************

app.route("/api/createOrder")
   .post(appControl.createOrder);

 app.route("/api/getOrderByCompanyId")
    .post(appControl.getOrderByCompanyId);

  app.route("/api/getAllOrders")
     .get(appControl.getAllOrders);

  app.route("/api/getOrderByQuery")
     .post(appControl.getOrderByQuery);

  app.route("/api/getOrderById")
     .post(appControl.getOrderById);

  app.route("/api/updateOrder")
     .post(appControl.updateOrder);

  app.route("/api/updateOrderPaymentStatus")
      .post(appControl.updateOrderPaymentStatus);

  app.route("/api/assignUpdate")
      .post(appControl.assignUpdate);

  app.route("/api/getUserOrder")
     .post(appControl.getUserOrder);

  app.route("/api/updateOrderTele")
    .post(appControl.updateOrderTele);

  app.route("/api/getOrderReport")
      .post(appControl.getOrderReport);

//***************************************  ORDER ROUTES ******************************************************************









};
