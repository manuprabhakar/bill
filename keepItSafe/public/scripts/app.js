var app = angular.module("app",['ngRoute','ui.router','ngMaterial','ngAnimate','LocalStorageModule','ngMdIcons','ngSanitize','mdDataTable','ngFileSaver']);

// app.config(function($routeProvider,$locationProvider) {
//
// 	 $routeProvider
//
//  .when("/login",{
// 	 templateURL : "login.html",
// 	 controller : "loginController"
//
//  })
//
//  .when("/logout",{
// 	 template : "ohh shit",
//
//  })
//
//
//  $locationProvider.html5Mode(true);
// });


app.config(function($stateProvider, $urlRouterProvider,$locationProvider) {

    $urlRouterProvider.otherwise('/404');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        .state('base', {
         abstract: true,
         url: '',
         templateUrl: 'views/base.html',
         controller:"loginController"
       })

        .state('logon', {
            url: '/logon',
            parent:'base',
            templateUrl: '/views/logon.html',
						controller:"loginController"
        })

        .state('createOrder', {
            url: '/createOrder',
            parent:'base',
            templateUrl: '/views/createOrder.html',
            controller:"createOrderController"
        })


      .state('teleDashboard', {
          url: '/teleDashboard',
          parent:'base',
          templateUrl: '/views/teleDashboard.html',
          controller:"teleDashboard"
      })

      .state('tele', {
          url: '/tele',
          parent:'base',
          templateUrl: '/views/tele.html',
          controller:"teleController"
      })



      .state('createOrderFinal', {
          url: '/createOrderFinal',
          parent:'base',
          templateUrl: '/views/createOrderFinal.html',
          controller:"createOrderFinalController"
      })

        .state('login', {
            url: '/login',
            templateUrl: '/views/login.html',
						controller:"loginController"
        })

// admin panel pages and redirects
        .state('adminBase', {
         abstract: true,
         url: '',
         templateUrl: 'views/adminBase.html',
         controller:"adminDashboardController"
          })

          .state('adminDashboard', {
              url: '/adminDashboard',
              parent:'adminBase',
              templateUrl: '/views/adminDashboard.html',
  						controller:"adminDashboardController"
          })

        .state('addProduct', {
            url: '/addProduct',
            parent:'adminBase',
            templateUrl: '/views/addProduct.html',
						controller:"productController"
        })


        .state('addCompany', {
            url: '/addCompany',
            parent:'adminBase',
            templateUrl: '/views/addCompany.html',
						controller:"companyController"
        })

        .state('expenses', {
            url: '/expenses',
            parent:'adminBase',
            templateUrl: '/views/expenses.html',
						controller:"expensesController"
        })

        .state('productAllotment', {
            url: '/productAllotment',
            parent:'adminBase',
            templateUrl: '/views/productAllotment.html',
            controller:"productAllotmentController"
        })

        .state('404', {
            url: '/404',
            templateUrl: '/views/404.html',
        })

        .state('allotment', {
            url: '/allotment',
            parent:'adminBase',
            templateUrl: '/views/allotment.html',
            controller:"allotmentController"
        })

        .state('payments', {
            url: '/payments',
            parent:'adminBase',
            templateUrl: '/views/payments.html',
            controller:"paymentsController"
        })

        .state('adminViewOrder', {
                    url: '/adminViewOrder',
                    parent:'adminBase',
                    templateUrl: '/views/adminViewOrder.html',
                    controller:"adminViewOrderController"
        })

        .state('adminEditOrder', {
              url: '/adminEditOrder',
              parent:'adminBase',
              templateUrl: '/views/adminEditOrder.html',
              controller:"adminViewOrderController"
        })


        .state('salesBase', {
         abstract: true,
         url: '',
         templateUrl: 'views/salesBase.html',
         controller:"loginController"
          })

        .state('salesDashboard', {
            url: '/salesDashboard',
            parent:'salesBase',
            templateUrl: '/views/salesDashboard.html',
            controller:"salesDashboardController"
        })

        .state('updateOrder', {
            url: '/updateOrder',
            parent:'salesBase',
            templateUrl: '/views/updateOrder.html',
            controller:"salesDashboardController"
        })

        .state('workshop', {
            url: '/workshop',
            parent:'salesBase',
            templateUrl: '/views/workshop.html',
            controller:"workshopController"
        })

				// $locationProvider.html5Mode(true);



});




app.run(function($rootScope, $http, $location,localStorageService){
      // keep user logged in after page refresh
      if (localStorageService.cookie.get('token')&&localStorageService.cookie.get('login')) {
          $http.defaults.headers.common.Authorization = localStorageService.cookie.get('login').token;
      }

      // redirect to login page if not logged in and trying to access a restricted page
      $rootScope.$on('$locationChangeStart', function (event, next, current) {
          var publicPages = ['/login','/404'];
          var restrictedPage = publicPages.indexOf($location.path()) === -1;
          if (restrictedPage&&(!localStorageService.cookie.get('token')||!localStorageService.cookie.get('login'))) {
              $location.path('/login');
          }
      });
  });






app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('teal')
    .accentPalette('purple');
});

app.config(function($compileProvider) {
    $compileProvider.preAssignBindingsEnabled(true);
  });

  app.config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        var m = moment(date);
        return m.isValid() ? m.format('DD/MM/YYYY'): '';
      };
  });
