app.controller("productAllotmentController",function($scope,apiService,$http,$mdToast,$state,$filter,localStorageService){


	(function() {
		var productCallback = apiService.showAllotments();
		productCallback.then(function(data) {
			$scope.productArray = data.data;
		});
	})();


   $scope.selectedItem;
			$scope.querySearch = function(query){
				return $http({
					method:"post",
					url:"/api/findProductQuery",
					data:{
						 name:query,
					},
					headers:{'Content-Type':'application/json'}
				}).then(function(data){
					 return data.data;
				});
    };


		$scope.updateRowCallback = function(rows){
					if(rows.length>1)
					 alert("cannot update multiple rows");

					 else
					 {
						 localStorageService.cookie.set("_id",rows[0],2);
             $state.go("allotment");
						}

					};


					$scope.product = {
					  requestedQuantity:null
					};


					$scope.createAllotmentProduct = function(){

	 			 				var productCallback = apiService.createAllotmentProduct($scope.selectedItem,$scope.product);
	 			 				  productCallback.then(function(data) {
	 			 				  	if(data.data.success)
	 			 						{
	 			 							$mdToast.show(
	 			 	 							 $mdToast.simple()
	 			 	 									 .content('Allotment added successfully!')
	 			 	 									 .hideDelay(3000)
	 			 	 					 );
										 $state.reload();

	 			 						}
	 			 				  });
	 			 			 };





});
