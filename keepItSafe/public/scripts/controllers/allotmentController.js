app.controller("allotmentController",function($scope,apiService,$http,$mdToast,$state,$filter,localStorageService){


$scope.roleType = ['vendor','merchant','container'];

$scope.id = localStorageService.cookie.get("_id");

$scope.updateLot = {
	id:$scope.id,idA:null,status:"recieved"
}




$scope.role = {
	type:"",name:""
};

$scope.allotment = {
	id : $scope.id,
	supplier:null,
	container:null,
	rate:null,
	quantityDispatched:null,
	mark:null,
	vendor:null,
	date:new Date(),
	product:null
};

$scope.updateRowCallback = function(rows){
			if(rows.length>1)
			 alert("cannot update multiple rows");

			 else
			 {
				  console.log(rows[0]);
          $scope.updateLot.idA = rows[0];
				 var callback = apiService.updateAllotmentByID($scope.updateLot);
					   callback.then(function(data) {
						 if(data.data.success)
						 {
							 $mdToast.show(
									$mdToast.simple()
											.content('Role added successfully!')
											.hideDelay(3000)
							);
							$state.reload();

						 }
					 });


				}

			};





	(function() {
		var productCallback = apiService.getAllotmentOption();
		productCallback.then(function(data) {
			$scope.roleArray = data.data;
		});
	})();



	(function() {
		  var productCallback = apiService.getAllotmentById($scope.id);
		  productCallback.then(function(data) {
            // $scope.product = data.data[0].product;
            $scope.allotment.product = data.data[0].product;
			$scope.allotmentArray = data.data[0].allotment;
			for(i in $scope.allotmentArray)
			{
				$scope.allotmentArray[i].created = $filter('date')($scope.allotmentArray[i].created, "d/MM/yyyy","+0530");
				$scope.allotmentArray[i].recieved = $filter('date')($scope.allotmentArray[i].recieved, "d/MM/yyyy","+0530");

			}
		});
	})();









					$scope.createAllotmentOption = function(){

	 			 				var productCallback = apiService.createAllotmentOption($scope.role);
	 			 				  productCallback.then(function(data) {
	 			 				  	if(data.data.success)
	 			 						{
	 			 							$mdToast.show(
	 			 	 							 $mdToast.simple()
	 			 	 									 .content('Role added successfully!')
	 			 	 									 .hideDelay(3000)
	 			 	 					 );
										 $state.reload();

	 			 						}
	 			 				  });
	 			 			 };



   $scope.updateAllotmentProduct = function() {

		 var productCallback = apiService.updateAllotmentProduct($scope.allotment);
			 productCallback.then(function(data) {
				 if(data.data.success)
				 {
					 $mdToast.show(
							$mdToast.simple()
									.content('Allotment added successfully!')
									.hideDelay(3000)
					);
					$state.reload();

				 }
			 });

   };





});
