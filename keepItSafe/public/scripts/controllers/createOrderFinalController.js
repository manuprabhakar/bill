app.controller("createOrderFinalController",function($scope,apiService,$http,$mdToast,$state,$filter,localStorageService,$q){
$scope.objectPush = {
  name:null,quantity:null
};

$scope.company = {
    name:'',phone:'',gstin:'',stateCode:'',state:''
  };
$scope.productArray1=[];
$scope.order = localStorageService.cookie.get("order");
$scope.stateCode=localStorageService.cookie.get("stateCode");
$scope.gstin=localStorageService.cookie.get("gstin");
$scope.address=localStorageService.cookie.get("address");
$scope.phone=localStorageService.cookie.get("phone");
$scope.state=localStorageService.cookie.get("state");
// console.log($scope.order);
$scope.pf=0;
$scope.ob='';
$scope.dt='';
$scope.po='';
$scope.cgstAmt=0;
var cgstSum=0;
$scope.sgstSum=0;
$scope.sgstAmt=0;
$scope.igstAmt=0;
var igstSum=0;
$scope.gst=0;
$scope.date;
$scope.contact='';
var wtax=0;
      var qtysum=0;

  $scope.selectedItem;
     $scope.querySearch = function(query){
       return $http({
         method:"post",
         url:"/api/findProductQuery",
         data:{
            name:query,
         },
         headers:{'Content-Type':'application/json'}
       }).then(function(data){
          return data.data;
       });
   };




 $scope.productArray=$scope.order.product;
 for(i in $scope.productArray )
 {
   delete $scope.productArray[i]._id;

 }

    $scope.pushToArray = function() {

      var temp = {
        name:$scope.objectPush.name,quantity:$scope.objectPush.quantity,rate:$scope.objectPush.rate
      };
      $scope.amt=0;
      $scope.amt=$scope.objectPush.quantity*$scope.objectPush.rate;

      if($scope.stateCode==7){
      $scope.igstSum="NA";
      $scope.cgstAmt=0;
      $scope.sgstAmt=0;
      $scope.cgstAmt=(.01*$scope.objectPush.tax/2)*$scope.amt;
      $scope.cgstAmt=$scope.cgstAmt.toFixed(1);
      $scope.order.stateCode=$scope.stateCode;
     // cgstSum+=parseFloat($scope.cgstAmt);
     // console.log($scope.cgstAmt);

      $scope.sgstAmt=$scope.cgstAmt;
      $scope.order.cgstAmt=$scope.cgstAmt;
      $scope.order.sgstAmt=$scope.sgstAmt;

      $scope.total=parseFloat($scope.amt)+parseFloat($scope.cgstAmt)+parseFloat($scope.sgstAmt);
      console.log($scope.total);
      var temp1 = {
        name:$scope.objectPush.name,hsn:$scope.objectPush.hsn,quantity:$scope.objectPush.quantity,rate:$scope.objectPush.rate,tax:$scope.objectPush.tax, amount:$scope.amt,cgst:$scope.objectPush.tax/2,cgstAmt:$scope.cgstAmt,sgst:$scope.objectPush.tax/2,sgstAmt:$scope.sgstAmt,igst:"NA", igstAmt:"NA",total:$scope.total
      };

    }
    else{
      cgstSum="NA";
      $scope.sgstSum="NA";
      $scope.igstAmt=(.01*$scope.objectPush.tax)*$scope.amt;
      $scope.igstAmt=$scope.igstAmt.toFixed(1);
      igstSum+=parseFloat($scope.igstAmt);
      //$scope.igstSum+=parseFloat($scope.igstAmt);
      //console.log($scope.igstSum);
      //$scope.gst=parseFloat($scope.igstSum);
      $scope.total=parseFloat($scope.amt)+parseFloat($scope.igstAmt);
      //console.log($scope.total);
      var temp1 = {
        name:$scope.objectPush.name,hsn:$scope.objectPush.hsn,quantity:$scope.objectPush.quantity,rate:$scope.objectPush.rate, tax:$scope.objectPush.tax, amount:$scope.amt,cgst:"NA",cgstAmt:"NA",sgst:"NA",sgstAmt:"NA",igst:$scope.objectPush.tax, igstAmt:$scope.igstAmt,total:$scope.total
      };
      // $scope.gst=$scope.gst.toFixed(2);
    }

    // console.log(temp);
    $scope.productArray.push(temp);
    $scope.productArray1.push(temp1);
    $scope.findTotal();
    // console.log($scope.productArray);

 };
 $scope.removeFromArray = function(x) {
  //  console.log(x);
    var index = $scope.productArray.indexOf(x.product);
    $scope.productArray.splice(index,1);
    $scope.productArray1.splice(index,1);
    $scope.findTotal();

 };
var sum;
$scope.findTotal = function() {
   sum = 0;
   qtysum=0;
   cgstSum=0;
   $scope.sgstSum=0;
   igstSum=0;
   wtax=0;
      for(i in $scope.productArray1)
      {
      //sum = sum+($scope.productArray1[i].amount+(.01*$scope.productArray1[i].tax*$scope.productArray1[i].amount));
      wtax= wtax+$scope.productArray1[i].amount;
      sum=sum+$scope.productArray1[i].total;
      qtysum=qtysum+$scope.productArray[i].quantity;
      }

      console.log(wtax);
    $scope.order.totalPrice = sum.toFixed(2);
    $scope.order.totalPrice = Math.round($scope.order.totalPrice);
    $scope.order.totalPrice+=parseFloat($scope.pf);
   if($scope.stateCode==7)
   { for(j in $scope.productArray1)
    {cgstSum=parseFloat(cgstSum)+parseFloat($scope.productArray1[j].cgstAmt);}
    cgstSum=parseFloat(cgstSum).toFixed(2);
    $scope.sgstSum=Math.round(cgstSum);
   }
   else
    { for(j in $scope.productArray1)
    {igstSum=parseFloat(igstSum)+parseFloat($scope.productArray1[j].igstAmt);}
    igstSum=parseFloat(igstSum).toFixed(2);
    igstSum=Math.round(igstSum);
    console.log(igstSum);
  }

  console.log(cgstSum);
  $scope.sgstSum=Math.round($scope.sgstSum);
  console.log($scope.sgstSum);


   $scope.order.createdAt=$scope.date;
};

$scope.placeOrder = function() {

  if ($scope.productArray.length<1) {
    alert("No Prducts Added to in order");
  }

  else{

  var request = [apiService.updateBalance($scope.order), apiService.createOrder($scope.order)];


   $q.all(request).then(function(data){
      if(data[0].data.success&&data[1].data.success)
      {
          // var callback = apiService.createPayment()

          //localStorageService.cookie.remove("order");
          alert("Order Placed  Successfully");
          //$state.go("createOrder");
          //console.log(data);
          $scope.bill();
      }
      else {
        alert("oops something went wrong! Please try again!!")
      }

   });
}
$scope.bill=function(){
  var callback = apiService.getOrderByCompanyId($scope.order.company_id);
  callback.then(function(data){
    if (data.data) {
      $scope.companyOrder=data.data;
      $scope.companyOrder[0].createdAt = $filter("date")($scope.companyOrder[0].createdAt,'dd/MM/yyyy');
      console.log($scope.companyOrder[0]);
      $scope.myOrder={
        po:$scope.po,ob:$scope.ob,address:$scope.address,contact:$scope.contact,phone:$scope.phone,gstin:$scope.company.gstin,
        stateCode:$scope.company.stateCode,state:$scope.company.state,productArray1:$scope.productArray1,qtysum:qtysum,cgstSum:cgstSum,
        igstSum:igstSum,dt:$scope.dt,pf:$scope.pf,name:$scope.company.name,wtax1:wtax,companyGST:$scope.gstin,companyAddress:$scope.address,
        companyStateCode:$scope.stateCode,companyState:$scope.state
      }
      console.log($scope.myOrder);
}});
}

};
$scope.placeOrderOriginal=function(){
  if($scope.stateCode==7){
    apiService.localBill($scope.companyOrder[0],$scope.myOrder);
  }
  else{
  apiService.igstBill($scope.companyOrder[0],$scope.myOrder);
  }
}
$scope.placeOrderDuplicate=function(){
  if($scope.stateCode==7){
    apiService.localBillDuplicate($scope.companyOrder[0],$scope.myOrder);
  }
  else{
  apiService.igstBillDuplicate($scope.companyOrder[0],$scope.myOrder);
  }

}
});
