app.controller("createOrderController",function($scope,$http,apiService,$filter,$state,localStorageService){

  $scope.routeArray = ['Loni', 'Sonia Vihar' ,
  'Johripur', 'Anand Parbat', 'Sagarpur', 'Noida',
  'Sultanpuri', 'Okhla', 'Vishwas Nagar','Karol Bagh', 'Mayapuri',
  'Brijbasan', 'Bawana', 'Dabri'];

  $scope.cpro = true;



  $scope.order = {
    company_id:null,
    companyName:null,
    totalPrice:null,
    createdAt:null,
    product:[]
  };


  // $scope.order = localStorageService.cookie.get("order");
  // console.log($scope.order);
  //  if($scope.order!=null)
  // {$scope.selectedItem = $scope.order.companyName;}

     $scope.querySearch = function(query){
       return $http({
         method:"post",
         url:"/api/findCompanyQuery",
         data:{
            name:query,

         },
         headers:{'Content-Type':'application/json'}
       }).then(function(data){
          if(data)
          return data.data;
          else {
            return [];
          }
       });
   };

  $scope.selectedItemChange = function(item){

    $scope.order.companyName = $scope.selectedItem.name;
    $scope.order.company_id = $scope.selectedItem._id;

    // console.log($scope.selectedItem.stateCode);
    localStorageService.cookie.set("stateCode",$scope.selectedItem.stateCode);
    localStorageService.cookie.set("gstin",$scope.selectedItem.gstin);
    localStorageService.cookie.set("address",$scope.selectedItem.address);
    localStorageService.cookie.set("phone",$scope.selectedItem.phone);
    localStorageService.cookie.set("state",$scope.selectedItem.state);

    $scope.cpro=false;
    //  console.log($scope.order);

      var callback = apiService.getOrderByCompanyId($scope.order.company_id);
      callback.then(function(data) {
        if (data.data) {

              $scope.companyOrder = data.data;
              for(i in $scope.companyOrder){
                $scope.companyOrder[i].createdAt = $filter("date")($scope.companyOrder[i].createdAt,'dd/MM/yyyy');
              }

              // console.log($scope.companyOrder);
              // if($scope.companyOrder[0]!=undefined)
              // {
              //   $scope.order.product = $scope.companyOrder[0].product;
              //   $scope.order.totalPrice = $scope.companyOrder[0].totalPrice;
              // }
              $scope.order.product=[];
              $scope.order.totalPrice=0;
        }


          // console.log($scope.order);
          localStorageService.cookie.set("order",$scope.order,10);

      });
  };



});
