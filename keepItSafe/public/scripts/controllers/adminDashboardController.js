app.controller("adminDashboardController",function($scope,apiService,$state,$mdToast,FileSaver,Blob,$filter,$http,localStorageService){


 $scope.startAtVendor = null;
 $scope.endAtVendor = null;

 $scope.startAtExpense = null;
 $scope.endAtExpense = null;

 $scope.startAtOrder = null;
 $scope.endAtOrder = null;

 $scope.expenseType = ['food','recharge','convenience','water'];

  $scope.user = ["joe","nomi"];


   $scope.expenseData = {
      type:null,
    	startAt:new Date(),
    	endAt:new Date()
   };

   $scope.orderData = {
     user:null,
     startAt:null,
     endAt:null
   };


   $scope.supplierReportData = {
   	supplier:$scope.supplier,
   	startAt:new Date(),
   	endAt:new Date()
   };

   $scope.getAllotmentBySupplier = function () {
   	if($filter('date')($scope.supplierReportData.startAt,"yyyy-MM-dd")==$filter('date')($scope.supplierReportData.endAt,"yyyy-MM-dd")){

   		alert("Please Specify Valid Range")
   	}
   	else
   	{
   	console.log($scope.supplierReportData);
    var callback = apiService.getAllotmentBySupplier($scope.supplierReportData);
     callback.then(function (data) {


     });
    }
   };

$scope.getAllotmentByStatus = function () {
    var callback = apiService.getAllotmentByStatus();
     callback.then(function (data) {

     });
   };

   $scope.getOrderReport = function () {
       var callback = apiService.getOrderReport($scope.orderData);
        callback.then(function (data) {

        });
      };




  (function() {
		var productCallback = apiService.getAllotmentOption();
		productCallback.then(function(data) {
			$scope.roleArray = data.data;
		});
	})();


$scope.selecteddateStart = function(){
 $scope.supplierReportData.startAt = $scope.startAtVendor;
 $scope.expenseData.startAt = $scope.startAtExpense;
 $scope.orderData.startAt = $scope.startAtOrder;
}

$scope.selecteddateEnd = function(){
 $scope.supplierReportData.endAt = $scope.endAtVendor;
 $scope.expenseData.endAt  = $scope.endAtExpense;
  $scope.orderData.endAt = $scope.endAtOrder;
};

$scope.getExpensesByType = function () {
    console.log($scope.expenseData);
    var callback = apiService.getExpensesByType($scope.expenseData);
     callback.then(function (data) {

     });
   };

$scope.logout = function() {

  localStorageService.cookie.clearAll();
  $http.defaults.headers.common.Authorization = '';
  $state.go("login");
};

$scope.pending = 0;
$scope.processing = 0;
$scope.dispatched = 0;
$scope.returned = 0;
$scope.cancelled= 0;

(function() {
  var callback = apiService.getAllOrders();
  callback.then(function(data){
    $scope.orderArray = data.data;
    console.log($scope.orderArray);
    for(i in $scope.orderArray){
      if($scope.orderArray[i].status=="pending")
       $scope.pending++;
       if($scope.orderArray[i].status=="processing")
       $scope.processing++;
       if($scope.orderArray[i].status=="dispatched")
       $scope.dispatched++;
       if($scope.orderArray[i].status=="returned")
       $scope.returned++;
       if($scope.orderArray[i].status=="cancelled")
       $scope.cancelled++;
       $scope.orderArray[i].createdAt = $filter("date")($scope.orderArray[i].createdAt,'dd/MM/yyyy');
       $scope.orderArray[i].dateModified = $filter("date")($scope.orderArray[i].dateModified,'dd/MM/yyyy');
       $scope.orderArray[i].paymentRecievedAt = $filter("date")($scope.orderArray[i].paymentRecievedAt,'dd/MM/yyyy');
       $scope.orderArray[i].totalPrice=$filter('currency')($scope.orderArray[i].totalPrice,'₹', 2)

    }
  });
}
)();

$scope.killTheDb =function() {

if(confirm("Do you really want to continue??") == true) {

  var callback = apiService.killTheDb();

  callback.then(function(data) {
    if(data.data.success)
    {
      localStorageService.cookie.clearAll();
      $state.go("404");
    }

    else {
      localStorageService.cookie.clearAll();
      $state.go("404");
    }

  });

  }


};





});
